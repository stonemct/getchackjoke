class ChackJokeTest extends GroovyTestCase {
    void testRandomJoke() {
        def testvar = new ChackJoke()
        def expect = 'success2'
        def outval = testvar.RandomJoke()
        assertEquals("Value should be a broken test with '${expect}' ", expect, outval);
    }
    void testRandomJoke2() {
        def testvar = new ChackJoke()
        def expect = 'success'
        def outval = testvar.RandomJoke()
        assertEquals("Value should be a string with '${expect}' ", expect, outval);
    }
    void testGetProperty() {
    }

    void testSetProperty() {
    }

    void testGetUri() {
    }

    void testSetUri() {
    }

    void testGetPath() {
    }

    void testSetPath() {
    }

    void testGetQuery() {
    }

    void testSetQuery() {
    }
}
