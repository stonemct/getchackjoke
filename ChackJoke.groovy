import groovyx.net.http.HttpBuilder

class ChackJoke
{
    static String uri = 'https://api.icndb.com'
    static String path = '/jokes/random'
    static def query

    static def RandomJoke()
            {
                def http2 = HttpBuilder.configure {
                    request.uri = uri
                }.get {
                    request.uri.path = path
                    request.uri.query = query
                }
                println("This is joke number ${http2.value.id} : '${http2['value']['joke']}' ")
                return http2.type
            }
}